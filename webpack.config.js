const { join } = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const { VueLoaderPlugin } = require('vue-loader');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const srcDir = join(__dirname, 'src');

module.exports = (env, argv) => {
    const devMode = argv.mode !== 'production';
    return {
        plugins: [
            new HtmlWebPackPlugin({
                template: join(srcDir, 'app.php'),
                filename: './app.php',
                hash: true
            }),
            new VueLoaderPlugin(),
            new MiniCssExtractPlugin({
                filename: devMode ? 'css/[name].css' : 'css/[name].[chunkhash].css'
            }),
            new CleanWebpackPlugin(['dist']),
            new OptimizeCSSPlugin()
        ],
        entry: {
            app: join(srcDir, 'app.js')
        },
        output: {
            path: join(__dirname, 'dist'),
            publicPath: '/dist/',
            filename: devMode ? 'js/[name].js' : 'js/[name].[chunkhash].js'
        },
        resolve: {
            extensions: ['.js', '.vue'],
            alias: {
                'vue$': 'vue/dist/vue.esm.js',
                '@': srcDir,
            }
        },
        optimization: {
            runtimeChunk: true,
            splitChunks: {
                chunks: 'all',
                cacheGroups: {
                    vendors: {
                        test: /node_modules/,
                        filename: 'js/[name].[chunkhash].js'
                    }
                }
            },
            minimizer: [new UglifyJsPlugin({sourceMap: true})]
        },
        module: {
            rules: [
                {
                    test: /\.(js|vue)$/,
                    exclude: /node_modules/,
                    loader: 'eslint-loader',
                    enforce: 'pre'
                },
                {
                    test: /\.vue$/,
                    loader: 'vue-loader'
                },
                // this will apply to both plain `.scss` files
                // AND `<style lang="scss">` blocks in `.vue` files
                {
                    test: /\.(css|scss)$/,
                    use: [
                        devMode ? 'vue-style-loader' : MiniCssExtractPlugin.loader,
                        'css-loader',
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: function () {
                                    return [
                                        require('autoprefixer')
                                    ];
                                }
                            }
                        },
                        'sass-loader'
                    ]
                },
                // this will apply to both plain `.js` files
                // AND `<script>` blocks in `.vue` files
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env'],
                            plugins: ['@babel/plugin-proposal-class-properties']
                        }
                    }
                }
            ]
        },
        devtool: 'source-map'
    }
};
