import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import shell from '@/shell';
import components from '@/components';

Vue.use(BootstrapVue);
Vue.use(components);

new Vue({
  el: '#app',
  render(h) {
    return h(shell, {
      props: {}
    });
  }
});
